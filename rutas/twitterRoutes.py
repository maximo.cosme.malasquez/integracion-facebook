from flask import Blueprint

from controllers.TwitterController import TwitterController
twitter = TwitterController()

from controllers.FootBallController import FootBallController
football = FootBallController()

twitterBP = Blueprint('twitterBP', __name__)

@twitterBP.route('/')
def hola():
    return twitter.hola()


@twitterBP.route('/prueba')
def prueba():
    return football.sendMessage()
